﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// 
/// </summary>
public class MasterSlideManager : MonoBehaviour
{

	#region Fields and Properties
    
	[SerializeField]
	GameObject environment;

	[SerializeField]
	Animator masterSlideAnim;

	[SerializeField]
	string OnCallToActionClicked_TriggerName;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
        DisableEnvironment();
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
       
    }

    #endregion

    #region Methods

	public void OnCallToActionButton_Clicked()
	{
        EnableEnvironment();
		masterSlideAnim.SetTrigger(OnCallToActionClicked_TriggerName);
	}
    
    public void DisableEnvironment()
	{
		environment.SetActive(false);

	}

    public void EnableEnvironment()
	{
		environment.SetActive(true);
	}

	#endregion
}
