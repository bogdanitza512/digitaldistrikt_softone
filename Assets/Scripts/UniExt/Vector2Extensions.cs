using System;

using UnityEngine;
using System.Collections;

namespace UniExt
{
    /// <summary>
    /// Extension methods for Unity.Color
    /// </summary>
    public static class Vector2Extensions
    {
        public static Vector2 With (this Vector2 original, float? x = null, float? y = null)
        {
            float newX = x.HasValue? x.Value : original.x;
            float newY = y.HasValue? y.Value : original.y;

            return new Vector2 (newX, newY);
        }

        public static Vector2 WithX(this Vector2 v, float x) {
            return new Vector2(x, v.y);
        }
	
        public static Vector2 WithY(this Vector2 v, float y) {
            return new Vector2(v.x, y);
        }
        
        public static Vector2 PlusX(this Vector2 vector, float plusX) {
        return new Vector2(vector.x + plusX, vector.y);
    }

        public static Vector2 PlusY(this Vector2 vector, float plusY) {
            return new Vector2(vector.x, vector.y + plusY);
        }

        public static Vector2 TimesX(this Vector2 vector, float timesX) {
            return new Vector2(vector.x * timesX, vector.y);
        }

        public static Vector2 TimesY(this Vector2 vector, float timesY) {
            return new Vector2(vector.x, vector.y * timesY);
        }

        public static Vector2 Rotate(this Vector2 vector, float degrees) {
            float sin = Mathf.Sin(degrees * Mathf.Deg2Rad);
            float cos = Mathf.Cos(degrees * Mathf.Deg2Rad);
            
            float tx = vector.x;
            float ty = vector.y;
            vector.x = (cos * tx) - (sin * ty);
            vector.y = (sin * tx) + (cos * ty);
            return vector;
        }

        public static Vector3 To3(this Vector2 v, float z) {
		    return new Vector3(v.x, v.y, z);
        }
    }
}
