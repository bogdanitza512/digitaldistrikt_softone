using System.Collections.Generic;
using UnityEngine;

namespace UniExt
{
    /// <summary>
    /// Extension methods for UnityEngine.Vector3.
    /// </summary>
    public static class Vector3Extensions
    {
        /// <summary>
        /// Finds the position closest to the given one.
        /// </summary>
        /// <param name="position">World position.</param>
        /// <param name="otherPositions">Other world positions.</param>
        /// <returns>Closest position.</returns>
        public static Vector3 GetClosest (this Vector3 position, IEnumerable<Vector3> otherPositions)
        {
            var closest = Vector3.zero;
            var shortestDistance = Mathf.Infinity;

            foreach (var otherPosition in otherPositions) {
                var distance = (position - otherPosition).sqrMagnitude;

                if (distance < shortestDistance) {
                    closest = otherPosition;
                    shortestDistance = distance;
                }
            }

            return closest;
        }

        public static Vector3 With (this Vector3 original, float? x = null, float? y = null, float? z = null)
        {
            float newX = x.HasValue? x.Value : original.x;
            float newY = y.HasValue? y.Value : original.y;
            float newZ = z.HasValue? z.Value : original.z;

            return new Vector3(newX, newY, newZ);
        }

        public static Vector2 XY(this Vector3 v) {
		    return new Vector2(v.x, v.y);
	    }

        public static Vector3 WithX(this Vector3 v, float x) {
            return new Vector3(x, v.y, v.z);
        }

        public static Vector3 WithY(this Vector3 v, float y) {
            return new Vector3(v.x, y, v.z);
        }

        public static Vector3 WithZ(this Vector3 v, float z) {
            return new Vector3(v.x, v.y, z);
        }

        public static Vector3 PlusX(this Vector3 vector, float plusX) {
            return new Vector3(vector.x + plusX, vector.y, vector.z);
        }

        public static Vector3 PlusY(this Vector3 vector, float plusY) {
            return new Vector3(vector.x, vector.y + plusY, vector.z);
        }

        public static Vector3 PlusZ(this Vector3 vector, float plusZ) {
            return new Vector3(vector.x, vector.y, vector.z + plusZ);
        }

        public static Vector3 TimesX(this Vector3 vector, float timesX) {
            return new Vector3(vector.x * timesX, vector.y, vector.z);
        }

        public static Vector3 TimesY(this Vector3 vector, float timesY) {
            return new Vector3(vector.x, vector.y * timesY, vector.z);
        }

        public static Vector3 TimesZ(this Vector3 vector, float timesZ) {
            return new Vector3(vector.x, vector.y, vector.z * timesZ);
        }
        
        // axisDirection - unit vector in direction of an axis (eg, defines a line that passes through zero)
        // point - the point to find nearest on line for
        public static Vector3 NearestPointOnAxis(this Vector3 axisDirection, Vector3 point, bool isNormalized = false)
        {
            if (!isNormalized) axisDirection.Normalize();
            var d = Vector3.Dot(point, axisDirection);
            return axisDirection * d;
        }

        // lineDirection - unit vector in direction of line
        // pointOnLine - a point on the line (allowing us to define an actual line in space)
        // point - the point to find nearest on line for
        public static Vector3 NearestPointOnLine(
            this Vector3 lineDirection, Vector3 point, Vector3 pointOnLine, bool isNormalized = false)
        {
            if (!isNormalized) lineDirection.Normalize();
            var d = Vector3.Dot(point - pointOnLine, lineDirection);
            return pointOnLine + (lineDirection * d);
        }

        public static Vector2 To2(this Vector3 vector) {
            return vector;
        }

        public static bool IsBehind(this Vector3 queried, Vector3 forward)
        {
            return Vector3.Dot(queried,forward) < 0f;
        }
    }
}
