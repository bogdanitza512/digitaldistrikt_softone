﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// 
/// </summary>
public class MobilityManager : MonoBehaviour
{

	#region Fields and Properties

    
	public VideoPlayerManager_Mobility videoPlayer;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
        
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
       
    }

	#endregion

	#region Methods

	public void StartPlayingVideo() { videoPlayer.Play_MobilityVideo(); }

    #endregion
}
