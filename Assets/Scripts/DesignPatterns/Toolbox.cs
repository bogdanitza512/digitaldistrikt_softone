﻿using UniExt;
using UnityEngine;

/// <summary>
/// 
/// </summary>
public class Toolbox : MonoSingleton<Toolbox>
{

    #region Fields and Properties



    #endregion

    #region Unity Messages

    /// <summary>
    /// Awake is called when the script instance is being loaded
    /// </summary>
    void Awake()
    {

    }

    #endregion

    #region Methods

    /// <summary>
    /// Initializes a new instance of the <see cref="T:Toolbox"/> class.
    /// Guarantee this will be always a singleton only - can't use the constructor!
    /// </summary>
    protected Toolbox() { }

    /// <summary>
    /// Allows runtime registration of global objects
    /// </summary>
    static public T RegisterComponent<T>() where T : Component
    {
        return Instance.GetOrAddComponent<T>();
    }

    #endregion
}
