﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Video;
using UnityEngine.UI;
using DG.Tweening;
using UniExt;

/// <summary>
/// 
/// </summary>
public class VideoPlayerManager_Mobility
	: MonoBehaviour
{

	#region Fields and Properties

	[SerializeField]
	VideoPlayer videoPlayer;

	[SerializeField]
	VideoClip Mobility_Video;

	[SerializeField]
	Animator videoProxyElementsAnim;

	[SerializeField]
	string Mobility_ElementsTriggerName;
   
	[SerializeField]
	RawImage screen;

	[SerializeField]
	Button playButton;


    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
		videoPlayer.prepareCompleted
				   += (source) =>
		{
			screen.DOFade(1, .5f);
			// playButton.image.color = playButton.image.color.WithAlpha(0);

		};
		videoPlayer.loopPointReached
				   += (source) =>
		{
			screen.DOFade(0, 2.5f);
			// playButton.image.DOFade(1, 2.5f);
		};

		//Play_MobilityVideo();
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
       
    }

	#endregion

	#region Methods

	public void StopVideoPlayer()
	{
		if(videoPlayer.isPlaying)
		    videoPlayer.Stop();
	}

	public void Play_MobilityVideo()
	{
		videoPlayer.clip = Mobility_Video;
		videoPlayer.Play();
		videoProxyElementsAnim.SetTrigger(Mobility_ElementsTriggerName);
	}

    #endregion
}
