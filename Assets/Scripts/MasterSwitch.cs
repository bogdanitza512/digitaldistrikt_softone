﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine.Events;

/// <summary>
/// 
/// </summary>
public class MasterSwitch : SerializedMonoBehaviour
{
	#region Nested Types

	public enum States { State_A = 0, State_B = 1}

	#endregion

	#region Fields and Properties

	[SerializeField]
	States currentState = States.State_B;

	[SerializeField]
	Animator switchAnim;

	[SerializeField]
	Dictionary<States, string> switchTriggerNameMap
	    = new Dictionary<States, string>()
	{
		{States.State_A,"SwitchToStateA_Trigger"},
		{States.State_B,"SwitchToStateB_Trigger"}
	};

	#endregion

	#region Unity Messages

	/// <summary>
	/// Start is called just before any of the Update methods is called the first time.
	/// </summary>
	void Start()
    {


    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
       
    }

    #endregion

    #region Methods

	public void OnSwitchToState(int _state)
	{
		var newState = (States)_state;
		if(newState != currentState)
		{
			currentState = newState;
			switchAnim.SetTrigger(switchTriggerNameMap[currentState]);
		}
		   
	}

    #endregion
}
