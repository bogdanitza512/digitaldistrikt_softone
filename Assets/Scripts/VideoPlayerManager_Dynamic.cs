﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Video;
using UnityEngine.UI;
using DG.Tweening;
using UniExt;

/// <summary>
/// 
/// </summary>
public class VideoPlayerManager_Dynamic : MonoBehaviour
{

	#region Fields and Properties

	[SerializeField]
	VideoPlayer videoPlayer;

	[SerializeField]
	VideoClip BAM_Video;

	[SerializeField]
	Animator videoProxyElementsAnim;

	[SerializeField]
	string BAM_ElementsTriggerName;

	[SerializeField]
    VideoClip EVA_Video;

    [SerializeField]
	string EVA_ElementsTriggerName;

	[SerializeField]
	RawImage screen;

	[SerializeField]
    Button playButton;

	[SerializeField]
	VideoClip currentClip;

	[SerializeField]
    Button BAM_Button;

    [SerializeField]
    Button EVA_Button;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
		
		videoPlayer.prepareCompleted
				   += (source) =>
		{
			screen.DOFade(1, .5f);
			// playButton.image.color = playButton.image.color.WithAlpha(0);
			// BAM_Button.interactable = false;
			// EVA_Button.interactable = false;
		};
		videoPlayer.loopPointReached
				   += (source) =>
		{
			screen.DOFade(0, 2.5f);

			// BAM_Button.interactable = true;
            // EVA_Button.interactable = true;
		};

		Play_BAMVideo();
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
       
    }

	#endregion

	#region Methods

	public void StopVideoPlayer()
	{
		if(videoPlayer.isPlaying)
		    videoPlayer.Stop();
	}

	public void Play_BAMVideo()
	{
		videoPlayer.clip = BAM_Video;
		videoPlayer.Play();
		videoProxyElementsAnim.SetTrigger(BAM_ElementsTriggerName);
	}

	public void Play_EVAVideo()
	{
		videoPlayer.clip = EVA_Video;
        videoPlayer.Play();
		videoProxyElementsAnim.SetTrigger(EVA_ElementsTriggerName);
	}

    #endregion
}
