﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using DG.Tweening;
using UniExt;

[ExecuteInEditMode]
/// <summary>
/// 
/// </summary>
public class CounterManager : MonoBehaviour
{

	#region Fields and Properties

	[SerializeField]
	TMP_Text text_saas;
	[SerializeField]
	string saasTemplate = "{0}.000 SaaS Installations";
	int saas = 0;
	int saasMax = 90;

	[SerializeField]
    TMP_Text text_users;
	[SerializeField]
	string usersTemplate = "{0}.000 users";
	int users = 0;
	int usersMax = 500;

	[SerializeField]
    TMP_Text text_requests;
	[SerializeField]
	string requestsTemplate = "{0} MIL requests";
	int requests = 0;
	int requestsMax = 260;

	[SerializeField]
    TMP_Text text_transfer;
	[SerializeField]
	string transferTemplate = "{0} TB data transfer";
	int transfer = 0;
	int transferMax = 9;

	float _lastAplha;

	[Range(0,1)]
	public float _Alpha;

	#endregion

	#region Unity Messages

	private void Start()
	{


	}
    
    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
		RefreshContent();
    }

	#endregion

	#region Methods

	public void RunCounter()
	{
		DOTween.To(() => saas,
                           x => { saas = x; text_saas.text = saasTemplate.FormatWith(saas); },
                           90, 5f)
                        .OnComplete(() => print("saas completed")).Play();
        DOTween.To(() => users,
                   x => { users = x; text_users.text = usersTemplate.FormatWith(users); },
                   500, 5f).SetEase(Ease.Linear)
               .OnComplete(() => print("users completed"));
        DOTween.To(() => requests,
                   x => { requests = x; text_requests.text = requestsTemplate.FormatWith(requests); },
                   260, 5f).SetEase(Ease.Linear)
               .OnComplete(() => print("requests completed"));
        DOTween.To(() => transfer,
                   x => { transfer = x; text_transfer.text = transferTemplate.FormatWith(transfer); },
                   9, 5f).SetEase(Ease.Linear)
               .OnComplete(() => print("transfer completed"));
	}

	public void RefreshContent()
	{
		if(System.Math.Abs(_Alpha - _lastAplha) > 0.01)
		{
			text_saas.text = saasTemplate.FormatWith(Mathf.CeilToInt(Mathf.Lerp(0, saasMax, _Alpha)));
			text_users.text = usersTemplate.FormatWith(Mathf.CeilToInt(Mathf.Lerp(0, usersMax, _Alpha)));
			text_requests.text = requestsTemplate.FormatWith(Mathf.CeilToInt(Mathf.Lerp(0, requestsMax, _Alpha)));
			text_transfer.text = transferTemplate.FormatWith(Mathf.CeilToInt(Mathf.Lerp(0, transferMax, _Alpha)));
			_lastAplha = _Alpha;
		}
	}


    #endregion
}
