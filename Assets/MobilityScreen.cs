﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ScreenMgr;
using UnityEngine.Video;

/// <summary>
/// 
/// </summary>
public class MobilityScreen : AnimatorScreen
{

    #region Fields and Properties

    [Header("Specific specs")]

    [SerializeField]
    VideoPlayerManager videoPlayerManager;

    [SerializeField]
    VideoClip clip;

    [SerializeField]
    Sprite splashScreen;

    [SerializeField]
    Animator proxyElementsAnim;

    [SerializeField]
    string proxyTriggerName;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {

    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {

    }

    #endregion

    #region Methods

    public void OnPlayButton_Clicked()
    {
        if (videoPlayerManager.videoPlayer.isPlaying || videoPlayerManager.isPaused)
        {
            videoPlayerManager.TogglePlayPause();
            return;
        }
        PlayVideo_Mobility();
    }

	public void PlayVideo_Mobility()
    {
        videoPlayerManager.PlayClip(clip);
        videoPlayerManager.SetSplashScreenSprite(splashScreen);
        proxyElementsAnim.SetTrigger(proxyTriggerName);
    }

    public override void OnAnimationIn()
    {
        base.OnAnimationIn();

        // PlayVideo_Mobility();
    }

    public override void OnAnimationOut()
    {
        base.OnAnimationOut();

        proxyElementsAnim.StopPlayback();
    }

    #endregion

}
